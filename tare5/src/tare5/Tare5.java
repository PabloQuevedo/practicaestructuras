
package tare5;

import java.util.Scanner;

/**
 *
 * @author PABLO Quevedo
 */
public class Tare5 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner sc= new Scanner(System.in);
    double dividendo ,divisor;
    System.out.println("Introduzca el dividendo: ");
   dividendo=sc.nextDouble();
   System.out.println("Introduzca el divisor: ");
   divisor=sc.nextDouble();
   if(divisor==0){
    System.out.println("No se puede dividir por cero ");
    
   }
   else {
    System.out.println(dividendo + " / " + divisor + " = "+dividendo/divisor);
  }
    
}
}