package primercl;
/**
 *
 * @author Pablo Quevedo
 */
import java.util.Scanner;

public class PrimerCl {
    
    public static void main(String[] args) {
         int n1,n2, suma , resta,producto, division;
     Scanner teclado = new Scanner(System.in);
     System.out.println("Introduzca el primer numero:");
     n1= teclado.nextInt();
     System.out.println("Introduzca el segundo numero:");
     n2= teclado.nextInt();
     suma= n1 + n2;
     resta= n1 - n2;
     producto= n1 * n2;
     division= n1 / n2;
     System.out.println("La suma de "+ n1 +" + "+ n2 +" es " + suma + ".");
     System.out.println("La resta de "+ n1 +" - "+ n2 +" es " + resta + ".");
     System.out.println("El producto de "+ n1 +" * "+ n2 +" es " + producto + ".");
     System.out.println("La division de "+ n1 +" / "+ n2 +" es " + division + ".");
    }
  }
