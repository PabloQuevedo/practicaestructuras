
package tarea6;
import javax.swing.JOptionPane;
/**
 *
 * @author PABLO Quevedo
 */
public class Tarea6 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
       String
               texto=JOptionPane.showInputDialog("Introduce un numero:");
       int numero=Integer.parseInt(texto);
       String binario=decimalBinario(numero);
       JOptionPane.showMessageDialog(null, binario);
       
    }
    public static String decimalBinario(int numero){
        String binario="";
        String digito;
        for(int i=numero;i>0;i/=2){
            if(i%2==1){
                digito="1"; 
            }
            else{
                digito="0";
            }
        binario=digito+binario;
        }
        return binario;
    }
}
