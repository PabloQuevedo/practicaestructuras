package tarea7;
import javax.swing.JOptionPane;
/**
 *
 * @author PABLO QUEVEDO
 */
public class Tarea7 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
     int number=0;
     String
             texto=JOptionPane.showInputDialog("Introduce el numero:");
       number=Integer.parseInt(texto);
       if(number>0){
           int numCifras=cuentaCifras(number);
           JOptionPane.showMessageDialog(null, numCifras);
           
       }
    }
    public static int cuentaCifras (int number){
        int count=0;
        for(int i=number;i>0; i/=10){
            count++;
        }
    return count;
    }
}
