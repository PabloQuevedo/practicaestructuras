

package tarea8;
import java.text.DecimalFormat;
import javax.swing.JOptionPane;
/**
 *
 * @author PABLO quevedo
 */
public class Tarea8 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
       String 
    t=JOptionPane.showInputDialog("Escribe una cantidad en bolivianos:");
     double c=Double.parseDouble(t);
      String
    m=JOptionPane.showInputDialog("Escriba la moneda a la que quieres convertir:");
      JOptionPane.showMessageDialog(null, convert(c,m));
    }     
    public static String convert(double c ,String m){
        
        double res=0;
     
        switch(m){
            case "libras":
                res=c*0.12;
                break;
            case "dolares":
                res=c*0.14;
                break;
            case "euros":
                res=c*0.13;
                break;
            default:
                JOptionPane.showMessageDialog(null, "error");
         }
    DecimalFormat df=new
            DecimalFormat("#.00");
    return c+ "Bolivianos en " +m+ "son: " +df.format(res);
    }
    
}
